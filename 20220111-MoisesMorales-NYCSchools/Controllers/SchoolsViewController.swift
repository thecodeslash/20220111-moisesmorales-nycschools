//
//  SchoolsViewController.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

protocol SchoolsViewControllerDelegate: AnyObject {
    func schoolsViewController(_ viewController: SchoolsViewController, didSelectSchool school: School)
    func fetchNextPage(for viewController: SchoolsViewController, shownSchools: [School])
}

final class SchoolsViewController: UIViewController {
    
    private enum Section: CaseIterable {
        case main
    }
    
    private typealias DataSource = UICollectionViewDiffableDataSource<Section, UICollectionViewListCell.Configuration>
    private typealias CellRegistration = UICollectionView.CellRegistration<UICollectionViewListCell, UICollectionViewListCell.Configuration>
    private typealias Snapshot = NSDiffableDataSourceSnapshot<Section, UICollectionViewListCell.Configuration>
    
    private lazy var collectionView: UICollectionView = {
        let configuration = UICollectionLayoutListConfiguration(appearance: .insetGrouped)
        let layout = UICollectionViewCompositionalLayout.list(using: configuration)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    private lazy var dataSource: DataSource = {
        let cellRegistration = CellRegistration { cell, indexPath, configuration in
            cell.configure(with: configuration)
        }
        
        return DataSource(collectionView: collectionView) { collectionView, indexPath, item in
            return collectionView.dequeueConfiguredReusableCell(using: cellRegistration, for: indexPath, item: item)
        }
    }()
    
    private var configuration: Configuration?
    weak var delegate: SchoolsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupNavigationItem()
        configuration.map(configure)
    }
    
    private func setupCollectionView() {
        view.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        collectionView.dataSource = dataSource
        collectionView.delegate = self
    }
    
    private func setupNavigationItem() {
        navigationItem.title = "NYC Schools"
        navigationItem.backButtonTitle = ""
        navigationItem.searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController?.searchBar.delegate = self
    }
    
    private func updateCollectionView(with configuration: Configuration) {
        var snapshot = Snapshot()
        snapshot.appendSections(Section.allCases)
        let configurations = configuration.schools.map { UICollectionViewListCell.Configuration(school: $0) }
        snapshot.appendItems(configurations, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: configuration.animatesDifferences)
    }
}

extension SchoolsViewController: Configurable {
    
    struct Configuration {
        let schools: [School]
        let animatesDifferences: Bool
    }
    
    func configure(with configuration: Configuration) {
        self.configuration = configuration
        
        guard isViewLoaded else { return }
        updateCollectionView(with: configuration)
    }
}

extension SchoolsViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        dataSource.itemIdentifier(for: indexPath).map { delegate?.schoolsViewController(self, didSelectSchool: $0.school) }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard navigationItem.searchController?.isActive == false else { return }
        
        let schools = configuration?.schools ?? []
        guard !schools.isEmpty, schools.count - 1 == indexPath.row else { return }
        delegate?.fetchNextPage(for: self, shownSchools: schools)
    }
}

extension SchoolsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let configuration = configuration else { return }
        let schools = searchText.isEmpty ? configuration.schools : configuration.schools.filter { $0.name?.lowercased().contains(searchText.lowercased()) == true }
        updateCollectionView(with: Configuration(schools: schools, animatesDifferences: false))
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        collectionView.isUserInteractionEnabled = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        collectionView.isUserInteractionEnabled = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        updateCollectionView(with: Configuration(schools: configuration?.schools ?? [], animatesDifferences: true))
    }
}
