//
//  SchoolDetailsViewController.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

final class SchoolDetailsViewController: UIViewController {
    
    private enum Section: CaseIterable {
        case main
    }
    
    private typealias DataSource = UITableViewDiffableDataSource<Section, SchoolDetailsTableViewCell.Configuration>
    private typealias Snapshot = NSDiffableDataSourceSnapshot<Section, SchoolDetailsTableViewCell.Configuration>
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        let nib = UINib(nibName: SchoolDetailsTableViewCell.name, bundle: SchoolDetailsTableViewCell.bundle)
        tableView.register(nib, forCellReuseIdentifier: SchoolDetailsTableViewCell.name)
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private lazy var dataSource: DataSource = {
        let dataSource = DataSource(tableView: tableView) { tableView, indexPath, configuration in
            let cell = tableView.dequeueReusableCell(withIdentifier: SchoolDetailsTableViewCell.name, for: indexPath) as? SchoolDetailsTableViewCell
            cell?.configure(with: configuration)
            return cell
        }
        
        dataSource.defaultRowAnimation = .fade
        return dataSource
    }()
    
    private var configuration: Configuration?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        configuration.map(configure)
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        tableView.dataSource = dataSource
    }
    
    private func updateTableView(with configuration: Configuration) {
        var snapshot = Snapshot()
        snapshot.appendSections(Section.allCases)
        let configurations = [SchoolDetailsTableViewCell.Configuration(school: configuration.school, sat: configuration.sat)]
        snapshot.appendItems(configurations, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: configuration.animatesDifferences)
    }
}

extension SchoolDetailsViewController: Configurable {
    
    struct Configuration {
        let school: School
        let sat: SAT?
        let animatesDifferences: Bool
    }
    
    func configure(with configuration: Configuration) {
        self.configuration = configuration
        
        guard isViewLoaded else { return }
        updateTableView(with: configuration)
    }
}
