//
//  SchoolsNetworkService.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import Foundation

protocol SchoolsNetworkServiceAPI: NetworkServiceAPI {
    var urlService: SchoolsURLServiceAPI { get }
    func fetchSchools(limit: Int, offset: Int) async throws -> [School]
    func fetchSAT(for school: School) async throws -> SAT?
}

struct SchoolsNetworkService: SchoolsNetworkServiceAPI {
    
    let session: URLSessionAPI
    let urlService: SchoolsURLServiceAPI
    
    init(session: URLSessionAPI = URLSession.shared, urlService: SchoolsURLServiceAPI = SchoolsURLService()) {
        self.session = session
        self.urlService = urlService
    }
    
    func fetchSchools(limit: Int, offset: Int) async throws -> [School] {
        guard let schoolsURL = urlService.schoolsURL(limit: limit, offset: offset) else { return [] }
        return try await fetchModel(ofType: [School].self, from: schoolsURL)
    }
    
    func fetchSAT(for school: School) async throws -> SAT? {
        guard let satURL = urlService.satURL(with: school.id) else { return nil }
        return try await fetchModel(ofType: [SAT].self, from: satURL).first
    }
}
