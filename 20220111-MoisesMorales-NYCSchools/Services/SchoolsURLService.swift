//
//  SchoolsURLService.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import Foundation

protocol SchoolsURLServiceAPI {
    func schoolsURL(limit: Int, offset: Int) -> URL?
    func satURL(with id: String?) -> URL?
}

struct SchoolsURLService: SchoolsURLServiceAPI {
    
    private var baseURLString: String { "https://data.cityofnewyork.us" }
    private var resourcePath: String { "/resource" }
    private var sharedURLString: String { baseURLString + resourcePath }
    
    func schoolsURL(limit: Int, offset: Int) -> URL? {
        return URL(string: "\(sharedURLString)/s3k6-pzi2.json?$limit=\(limit)&$offset=\(offset)&$order=dbn")
    }
    
    func satURL(with id: String?) -> URL? {
        guard let id = id else { return nil }
        return URL(string: "\(sharedURLString)/f9bf-2cp4.json?dbn=\(id)")
    }
}
