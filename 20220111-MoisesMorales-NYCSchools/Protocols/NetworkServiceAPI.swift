//
//  NetworkServiceAPI.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

protocol NetworkServiceAPI {
    var session: URLSessionAPI { get }
    func fetchModel<Model: Decodable>(ofType type: Model.Type, from url: URL) async throws -> Model
}

extension NetworkServiceAPI {
    
    func fetchModel<Model: Decodable>(ofType type: Model.Type, from url: URL) async throws -> Model {
        let result: (data: Data, response: URLResponse) = try await session.data(from: url, delegate: nil)
        return try JSONDecoder().decode(type, from: result.data)
    }
}
