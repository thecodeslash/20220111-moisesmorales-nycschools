//
//  CoordinatorAPI.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

protocol CoordinatorAPI: AnyObject {
    var children: [CoordinatorAPI] { get }
    func start()
}

protocol RootCoordinatorAPI: CoordinatorAPI {
    var window: UIWindow { get }
}

protocol NavigationCoordinatorAPI: CoordinatorAPI {
    var navigationController: UINavigationController { get }
}
