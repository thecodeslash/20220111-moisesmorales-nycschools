//
//  Configurable.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import Foundation

protocol Configurable: AnyObject {
    associatedtype Configuration
    func configure(with configuration: Configuration)
}
