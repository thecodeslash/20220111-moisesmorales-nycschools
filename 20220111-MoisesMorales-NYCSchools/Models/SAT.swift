//
//  SAT.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import Foundation

struct SAT: Codable, Hashable {
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case criticalReadingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
    
    let id: String?
    let criticalReadingScore: String?
    let mathScore: String?
    let writingScore: String?
    
    var isValidSAT: Bool {
        return criticalReadingScore?.isNumber == true && mathScore?.isNumber == true && writingScore?.isNumber == true
    }
}
