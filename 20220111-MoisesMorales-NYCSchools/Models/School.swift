//
//  School.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import Foundation

struct School: Codable, Hashable {
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case street = "primary_address_line_1"
        case city
        case state = "state_code"
        case zip
        case overview = "overview_paragraph"
        case phone = "phone_number"
        case _email = "school_email"
        case _website = "website"
    }
    
    let id: String?
    let name: String?
    private let street: String?
    private let city: String?
    private let state: String?
    private let zip: String?
    let overview: String?
    let phone: String?
    private let _email: String?
    private let _website: String?
    
    var address: String? {
        guard let street = street?.capitalized,
              let city = city?.capitalized,
              let state = state?.uppercased(),
              let zip = zip, zip.isNumber else { return nil }
        
        return "\(street), \(city), \(state) \(zip)"
    }
    
    var email: String? { _email?.lowercased() }
    var website: String? { _website?.lowercased() }
}
