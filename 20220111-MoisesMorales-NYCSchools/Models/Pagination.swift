//
//  Pagination.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import Foundation

struct Pagination {
    
    let limit: Int
    let offset: Int
    let hasNextPage: Bool
    
    init(limit: Int, offset: Int, hasNextPage: Bool) {
        self.limit = limit < 0 ? 0 : limit
        self.offset = offset < 0 ? 0 : offset
        self.hasNextPage = hasNextPage
    }
    
    var next: Pagination {
        return Pagination(limit: limit, offset: offset + limit, hasNextPage: hasNextPage)
    }
    
    var last: Pagination {
        return Pagination(limit: limit, offset: offset, hasNextPage: false)
    }
}
