//
//  SceneDelegate.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var rootCoordinator: SchoolsRootCoordinatorAPI?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = scene as? UIWindowScene else { return }
        let window = UIWindow(windowScene: scene)
        
        rootCoordinator = SchoolsRootCoordinator(window: window)
        rootCoordinator?.start()
    }
}
