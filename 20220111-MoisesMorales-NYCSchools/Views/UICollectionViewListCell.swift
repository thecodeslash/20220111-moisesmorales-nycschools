//
//  UICollectionViewListCell.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

extension UICollectionViewListCell: Configurable {
    
    struct Configuration: Hashable {
        let school: School
    }
    
    func configure(with configuration: Configuration) {
        var contentConfiguration = defaultContentConfiguration()
        
        contentConfiguration.text = configuration.school.name
        contentConfiguration.textProperties.font = .systemFont(ofSize: 17)
        contentConfiguration.textProperties.color = .label
        
        contentConfiguration.secondaryText = configuration.school.address
        contentConfiguration.secondaryTextProperties.font = .systemFont(ofSize: 14)
        contentConfiguration.secondaryTextProperties.color = .secondaryLabel
        
        self.contentConfiguration = contentConfiguration
        
        accessories = [.disclosureIndicator()]
    }
}
