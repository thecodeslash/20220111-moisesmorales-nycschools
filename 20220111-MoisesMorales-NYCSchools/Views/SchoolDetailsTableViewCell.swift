//
//  SchoolDetailsTableViewCell.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

class SchoolDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet private var rootStackView: UIStackView!
    @IBOutlet private var satScoresStackView: UIStackView!
    @IBOutlet private var phoneStackView: UIStackView!
    @IBOutlet private var emailStackView: UIStackView!
    @IBOutlet private var websiteStackView: UIStackView!
    @IBOutlet private var addressStackView: UIStackView!
    
    @IBOutlet private var overviewLabel: UILabel!
    @IBOutlet private var criticalReadingScoreLabel: UILabel!
    @IBOutlet private var mathScoreLabel: UILabel!
    @IBOutlet private var writingScoreLabel: UILabel!
    @IBOutlet private var phoneLabel: TappableLabel!
    @IBOutlet private var emailLabel: TappableLabel!
    @IBOutlet private var websiteLabel: TappableLabel!
    @IBOutlet private var addressLabel: TappableLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let spacing: CGFloat = 20
        rootStackView.setCustomSpacing(spacing, after: overviewLabel)
        rootStackView.setCustomSpacing(spacing, after: satScoresStackView)
    }
}

extension SchoolDetailsTableViewCell: Configurable {
    
    struct Configuration: Hashable {
        let school: School
        let sat: SAT?
    }
    
    func configure(with configuration: Configuration) {
        overviewLabel.text = configuration.school.overview
        criticalReadingScoreLabel.text = "Critical Reading: \(configuration.sat?.criticalReadingScore ?? "")"
        mathScoreLabel.text = "Math: \(configuration.sat?.mathScore ?? "")"
        writingScoreLabel.text = "Writing: \(configuration.sat?.writingScore ?? "")"
        phoneLabel.text = configuration.school.phone
        emailLabel.text = configuration.school.email
        websiteLabel.text = configuration.school.website
        addressLabel.text = configuration.school.address
        
        overviewLabel.isHidden = configuration.school.overview.isNilOrEmpty
        satScoresStackView.isHidden = configuration.sat?.isValidSAT == false || configuration.sat == nil
        phoneStackView.isHidden = configuration.school.phone.isNilOrEmpty
        emailStackView.isHidden = configuration.school.email.isNilOrEmpty
        websiteStackView.isHidden = configuration.school.website.isNilOrEmpty
        addressStackView.isHidden = configuration.school.address.isNilOrEmpty
    }
}
