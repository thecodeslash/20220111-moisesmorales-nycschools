//
//  TappableLabel.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit
import MapKit

class TappableLabel: UILabel {
    
    override var text: String? {
        didSet {
            guard let text = text else { return }
            let attributedText = NSMutableAttributedString(string: text)
            let range = NSRange(location: 0, length: text.count)
            attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
            self.attributedText = attributedText
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        enableTapping()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        enableTapping()
    }
    
    private func enableTapping() {
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(labelTapped)))
    }
    
    @objc private func labelTapped() {
        guard let text = text, !text.isEmpty, let detector = try? NSDataDetector(types: NSTextCheckingAllTypes),
              let match = detector.matches(in: text, options: [], range: NSRange(location: 0, length: text.count)).first else { return }
        
        switch match.resultType {
        case .link:
            let string = text.contains("@") ? "mailto:\(text)" : text.contains("http") ? text : "https://\(text)"
            guard let url = URL(string: string) else { break }
            UIApplication.shared.open(url, options: [:])
        case .address:
            let geocoder = CLGeocoder()
            geocoder.geocodeAddressString(text) { placemarks, error in
                guard let placemark = placemarks?.first,
                      let coordinate = placemark.location?.coordinate else { return }
                
                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate))
                mapItem.name = "Destination"
                mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
            }
        case .phoneNumber:
            guard let url = URL(string: "tel://\(text)") else { return }
            UIApplication.shared.open(url, options: [:])
        default: break
        }
    }
}
