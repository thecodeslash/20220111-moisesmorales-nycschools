//
//  UIViewController.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

fileprivate final class StackView: UIStackView {
    
    convenience init(message: String) {
        self.init()
        axis = .vertical
        alignment = .center
        
        spacing = 20
        isLayoutMarginsRelativeArrangement = true
        layoutMargins = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        backgroundColor = .systemGray5
        layer.cornerRadius = 20
        
        let activityIndicatorView = UIActivityIndicatorView(style: .large)
        activityIndicatorView.startAnimating()
        
        let messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.boldSystemFont(ofSize: 18)
        messageLabel.numberOfLines = 0
        
        [activityIndicatorView, messageLabel].forEach(addArrangedSubview)
    }
}

fileprivate final class ContainerView: UIView {
    
    convenience init(message: String) {
        self.init()
        backgroundColor = .clear
        
        let stackView = StackView(message: message)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
            stackView.heightAnchor.constraint(lessThanOrEqualToConstant: 250)
        ])
    }
}

extension UIViewController {
    
    func showAlert(title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(okAction)
        present(alertController, animated: true)
    }
    
    func showActivityIndicator(with message: String) {
        hideActivityIndicator()
        
        let containerView = ContainerView(message: message)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.topAnchor),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    func hideActivityIndicator() {
        view.subviews.filter { $0 is ContainerView }.forEach { $0.removeFromSuperview() }
    }
}

