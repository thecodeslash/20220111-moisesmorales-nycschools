//
//  UIView.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

extension UIView {
    static var name: String { String(describing: self) }
    static var bundle: Bundle { Bundle(for: self) }
}
