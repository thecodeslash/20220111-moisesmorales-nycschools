//
//  SchoolsRootCoordinator.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

protocol SchoolsRootCoordinatorAPI: RootCoordinatorAPI {
    var networkService: SchoolsNetworkServiceAPI { get }
}

final class SchoolsRootCoordinator: SchoolsRootCoordinatorAPI {
    
    let window: UIWindow
    let networkService: SchoolsNetworkServiceAPI
    var children: [CoordinatorAPI] = []
    
    init(window: UIWindow, networkService: SchoolsNetworkServiceAPI = SchoolsNetworkService()) {
        self.window = window
        self.networkService = networkService
    }
    
    func start() {
        let coordinator = SchoolsCoordinator(networkService: networkService)
        
        window.rootViewController = coordinator.navigationController
        window.makeKeyAndVisible()
        
        children.append(coordinator)
        coordinator.start()
    }
}
