//
//  SchoolsCoordinator.swift
//  20220111-MoisesMorales-NYCSchools
//
//  Created by Moises Morales on 1/11/22.
//

import UIKit

protocol SchoolsCoordinatorAPI: NavigationCoordinatorAPI {
    var networkService: SchoolsNetworkServiceAPI { get }
    var pagination: Pagination { get }
}

final class SchoolsCoordinator: SchoolsCoordinatorAPI {
    
    let networkService: SchoolsNetworkServiceAPI
    var pagination: Pagination
    let navigationController: UINavigationController
    var children: [CoordinatorAPI] = []
    
    init(networkService: SchoolsNetworkServiceAPI, pagination: Pagination = Pagination(limit: 50, offset: 0, hasNextPage: true)) {
        self.networkService = networkService
        self.pagination = pagination
        let schoolsViewController = SchoolsViewController()
        navigationController = UINavigationController(rootViewController: schoolsViewController)
        
        navigationController.navigationBar.prefersLargeTitles = true
        schoolsViewController.delegate = self
    }
    
    func start() {
        guard let schoolsViewController = navigationController.topViewController as? SchoolsViewController else { return }
        schoolsViewController.showActivityIndicator(with: "Loading schools...")
        
        Task {
            do {
                let schools = try await networkService.fetchSchools(limit: pagination.limit, offset: pagination.offset)
                let configuration = SchoolsViewController.Configuration(schools: schools, animatesDifferences: true)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    schoolsViewController.hideActivityIndicator()
                    schoolsViewController.configure(with: configuration)
                }
            } catch {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    schoolsViewController.hideActivityIndicator()
                    schoolsViewController.showAlert(title: "Error", message: error.localizedDescription)
                }
            }
        }
    }
}

extension SchoolsCoordinator: SchoolsViewControllerDelegate {
    
    func schoolsViewController(_ viewController: SchoolsViewController, didSelectSchool school: School) {
        let schoolDetailsViewController = SchoolDetailsViewController()
        schoolDetailsViewController.navigationItem.title = school.name
        navigationController.pushViewController(schoolDetailsViewController, animated: true)
        
        Task {
            let sat = try? await networkService.fetchSAT(for: school)
            let configuration = SchoolDetailsViewController.Configuration(school: school, sat: sat, animatesDifferences: true)
            await MainActor.run { schoolDetailsViewController.configure(with: configuration) }
        }
    }
    
    func fetchNextPage(for viewController: SchoolsViewController, shownSchools: [School]) {
        guard pagination.hasNextPage else { return }
        pagination = pagination.next
        
        Task {
            guard let newSchools = try? await networkService.fetchSchools(limit: pagination.limit, offset: pagination.offset) else { return }
            
            if newSchools.isEmpty {
                pagination = pagination.last
                return
            }
            
            let configuration = SchoolsViewController.Configuration(schools: shownSchools + newSchools, animatesDifferences: false)
            await MainActor.run { viewController.configure(with: configuration) }
        }
    }
}
