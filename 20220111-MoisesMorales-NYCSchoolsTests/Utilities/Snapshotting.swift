//
//  Snapshotting.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import UIKit
import SnapshotTesting

extension Snapshotting where Value == UIViewController, Format == UIImage {
    static var image99Percent: Snapshotting { return .image(drawHierarchyInKeyWindow: true, precision: 0.99) }
}
