//
//  ResourceLoader.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import Foundation
@testable import _0220111_MoisesMorales_NYCSchools

final class ResourceLoader {
    
    static var schools: [School] { model(ofType: [School].self, jsonName: "schools") ?? [] }
    static var sats: [SAT] { model(ofType: [SAT].self, jsonName: "sats") ?? [] }
        
    private static func model<Model: Decodable>(ofType type: Model.Type, jsonName: String) -> Model? {
        guard let path = Bundle(for: ResourceLoader.self).path(forResource: jsonName, ofType: "json") else { return nil }
        let url = URL(fileURLWithPath: path)
        
        guard let data = try? Data(contentsOf: url) else { return nil }
        return try? JSONDecoder().decode(type, from: data)
    }
}

