//
//  MockURLSession.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import Foundation
@testable import _0220111_MoisesMorales_NYCSchools

struct MockURLSession: URLSessionAPI {
    
    func data(from url: URL, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse) {
        if url.absoluteString.hasPrefix("https://data.cityofnewyork.us/resource/s3k6-pzi2") {
            return (try JSONEncoder().encode(ResourceLoader.schools), URLResponse())
        } else if url.absoluteString.hasPrefix("https://data.cityofnewyork.us/resource/f9bf-2cp4") {
            return (try JSONEncoder().encode(ResourceLoader.sats), URLResponse())
        } else {
            return (Data(), URLResponse())
        }
    }
}

