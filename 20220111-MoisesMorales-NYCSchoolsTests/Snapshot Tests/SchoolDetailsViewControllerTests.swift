//
//  SchoolDetailsViewControllerTests.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import XCTest
import SnapshotTesting
@testable import _0220111_MoisesMorales_NYCSchools

final class SchoolDetailsViewControllerTests: XCTestCase {
    
    func testLightMode() {
        assertSnapshot(matching: viewController(with: .light), as: .image99Percent)
    }
    
    func testDarkMode() {
        assertSnapshot(matching: viewController(with: .dark), as: .image99Percent)
    }
    
    private func viewController(with style: UIUserInterfaceStyle) -> UIViewController {
        let viewController = SchoolDetailsViewController()
        
        guard let school = ResourceLoader.schools.first else { fatalError("Failed to decode.") }
        let configuration = SchoolDetailsViewController.Configuration(school: school, sat: ResourceLoader.sats.first, animatesDifferences: false)
        
        viewController.configure(with: configuration)
        viewController.overrideUserInterfaceStyle = style
        
        return viewController
    }
}
