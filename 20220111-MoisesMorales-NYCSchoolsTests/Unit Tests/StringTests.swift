//
//  StringTests.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import XCTest
@testable import _0220111_MoisesMorales_NYCSchools

final class StringTests: XCTestCase {
        
    func testIsNumber() {
        let string1 = "03430"
        let string2 = ""
        let string3 = "  "
        let string4 = "H2A5"
        let string5 = "33902"
        
        XCTAssertTrue(string1.isNumber)
        XCTAssertFalse(string2.isNumber)
        XCTAssertFalse(string3.isNumber)
        XCTAssertFalse(string4.isNumber)
        XCTAssertTrue(string5.isNumber)
    }
}
