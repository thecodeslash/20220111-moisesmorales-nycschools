//
//  SchoolTests.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import XCTest
@testable import _0220111_MoisesMorales_NYCSchools

final class SchoolTests: XCTestCase {
    
    //These tests are here to ensure proper string formatting
    
    private lazy var school = ResourceLoader.schools.first
    
    func testAddress() {
        XCTAssertEqual(school?.address, "525 East Houston Street, Manhattan, NY 10002")
    }
    
    func testEmail() {
        XCTAssertEqual(school?.email, "apply@bhsec.bard.edu")
    }
    
    func testWebsite() {
        XCTAssertEqual(school?.website, "www.bard.edu/bhsec")
    }
}
