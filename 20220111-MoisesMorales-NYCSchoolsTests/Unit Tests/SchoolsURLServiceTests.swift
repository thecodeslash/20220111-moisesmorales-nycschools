//
//  SchoolsURLServiceTests.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import XCTest
@testable import _0220111_MoisesMorales_NYCSchools

final class SchoolsURLServiceTests: XCTestCase {
    
    private lazy var urlService = SchoolsURLService()
    
    func testSchoolsURL() {
        XCTAssertEqual(urlService.schoolsURL(limit: 50, offset: 0)?.absoluteString, "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=50&$offset=0&$order=dbn")
        XCTAssertEqual(urlService.schoolsURL(limit: 100, offset: 150)?.absoluteString, "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$limit=100&$offset=150&$order=dbn")
    }
    
    func testSatURL() {
        XCTAssertEqual(urlService.satURL(with: "01M696")?.absoluteString, "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=01M696")
        XCTAssertEqual(urlService.satURL(with: "1Z922M")?.absoluteString, "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=1Z922M")
    }
}
