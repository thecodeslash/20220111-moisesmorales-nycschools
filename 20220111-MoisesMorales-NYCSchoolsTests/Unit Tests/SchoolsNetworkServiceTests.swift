//
//  SchoolsNetworkServiceTests.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import XCTest
@testable import _0220111_MoisesMorales_NYCSchools

final class SchoolsNetworkServiceTests: XCTestCase {
    
    private lazy var networkService = SchoolsNetworkService(session: MockURLSession(), urlService: SchoolsURLService())
    
    func testFetchSchools() async throws {
        let schools = try await networkService.fetchSchools(limit: 50, offset: 0)
        XCTAssertEqual(schools.count, 1)
        XCTAssertEqual(schools.first?.id, "01M696")
    }
    
    func testFetchSAT() async throws {
        guard let school = ResourceLoader.schools.first else { fatalError("Failed to decode.") }
        let sat = try await networkService.fetchSAT(for: school)
        
        XCTAssertNotNil(sat)
        XCTAssertEqual(sat?.id, "01M696")
    }
}
