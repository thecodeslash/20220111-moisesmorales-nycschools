//
//  SATTests.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import XCTest
@testable import _0220111_MoisesMorales_NYCSchools

final class SATTests: XCTestCase {
        
    func testIsValidSAT() {
        let sat1 = ResourceLoader.sats.first
        XCTAssertTrue(sat1?.isValidSAT == true)
        
        let sat2 = SAT(id: "01M696", criticalReadingScore: "s", mathScore: "s", writingScore: "s")
        XCTAssertFalse(sat2.isValidSAT)
        
        let sat3 = SAT(id: "A1Z049", criticalReadingScore: "500", mathScore: "400", writingScore: "300z")
        XCTAssertFalse(sat3.isValidSAT)
    }
}
