//
//  OptionalTests.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import XCTest
@testable import _0220111_MoisesMorales_NYCSchools

final class OptionalTests: XCTestCase {
        
    func testIsNilOrEmpty() {
        let string1: String? = ""
        let string2: String? = nil
        let string3: String? = "Hello World"
        let string4: String? = "  "
        
        XCTAssertTrue(string1.isNilOrEmpty)
        XCTAssertTrue(string2.isNilOrEmpty)
        XCTAssertFalse(string3.isNilOrEmpty)
        XCTAssertFalse(string4.isNilOrEmpty)
    }
}
