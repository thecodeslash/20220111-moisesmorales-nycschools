//
//  PaginationTests.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import XCTest
@testable import _0220111_MoisesMorales_NYCSchools

final class PaginationTests: XCTestCase {
    
    private lazy var pagination = Pagination(limit: 50, offset: 0, hasNextPage: true)
    
    func testPaginationInit() {
        let pagination = Pagination(limit: -2, offset: -299, hasNextPage: true)
        XCTAssertEqual(pagination.limit, 0)
        XCTAssertEqual(pagination.offset, 0)
    }
    
    func testPaginationNext() {
        pagination = pagination.next
        XCTAssertEqual(pagination.limit, 50)
        XCTAssertEqual(pagination.offset, 50)
        
        pagination = pagination.next
        XCTAssertEqual(pagination.limit, 50)
        XCTAssertEqual(pagination.offset, 100)
    }
    
    func testPaginationLast() {
        XCTAssertTrue(pagination.hasNextPage)
        pagination = pagination.last
        XCTAssertFalse(pagination.hasNextPage)
    }
}
