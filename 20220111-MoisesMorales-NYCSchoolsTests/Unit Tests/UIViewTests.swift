//
//  UIViewTests.swift
//  20220111-MoisesMorales-NYCSchoolsTests
//
//  Created by Moises Morales on 1/12/22.
//

import XCTest
@testable import _0220111_MoisesMorales_NYCSchools

final class UIViewTests: XCTestCase {
        
    func testName() {
        XCTAssertEqual(SchoolDetailsTableViewCell.name, "SchoolDetailsTableViewCell")
    }
    
    func testBundle() {
        XCTAssertEqual(SchoolDetailsTableViewCell.bundle, Bundle(for: SchoolDetailsTableViewCell.self))
    }
}
